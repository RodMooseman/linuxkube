oc delete imagestream react-web-app-builder static-web-app-running-on-nginx web-app-s2i-image nginx-image-runtime -n $1 
oc delete buildconfig react-web-app-builder static-web-app-running-on-nginx   -n $1
oc delete deploymentconfig react-web-app -n $1
oc delete service react-web-app -n $1
oc delete route react-web-app -n $1
